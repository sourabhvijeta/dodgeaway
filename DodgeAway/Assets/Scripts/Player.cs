﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public float playerYOffset;
	Vector3 position;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButton (0)) {
			transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, transform.position.y + playerYOffset, 10.0f));

			position = transform.position;
			position.x = Mathf.Clamp (position.x, -2.2f, 2.2f);
			transform.position = position;

		}	
	}
}
